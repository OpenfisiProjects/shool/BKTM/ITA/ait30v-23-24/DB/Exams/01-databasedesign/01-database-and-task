## 1. 

-----------------------------

Der Code für die Datenbank erfüllt einige der Anforderungen aus der PDF, aber nicht alle. Hier sind einige Punkte, die zu beachten sind:

- Der Code erstellt eine Datenbank namens DBS, aber die PDF gibt keinen Namen für die Datenbank an. Es wäre besser, einen aussagekräftigeren Namen zu wählen, z.B. SchulrechnerDB.
- Der Code verwendet verschiedene Datentypen für die Attribute, aber die PDF gibt keine Datentypen an. Es wäre besser, die Datentypen zu begründen oder zu erklären, warum sie gewählt wurden.
- Der Code legt die Seriennummer als Primärschlüssel für die Tabelle Rechner fest, aber die PDF sagt nicht, ob die Seriennummer eindeutig ist oder nicht. Es wäre besser, zu überprüfen, ob es Duplikate gibt oder eine andere Möglichkeit zu finden, die Rechner zu identifizieren.
- Der Code legt die Raumnummer als Primärschlüssel für die Tabelle Raum fest, aber die PDF sagt nicht, ob die Raumnummer eindeutig ist oder nicht. Es wäre besser, zu überprüfen, ob es Duplikate gibt oder eine andere Möglichkeit zu finden, die Räume zu identifizieren.
- Der Code legt die ID als Primärschlüssel für die Tabelle Benutzer fest, aber die PDF sagt nicht, wie die ID generiert wird oder ob sie eindeutig ist oder nicht. Es wäre besser, zu erklären, wie die ID vergeben wird oder eine andere Möglichkeit zu finden, die Benutzer zu identifizieren.
- Der Code legt die SessionID als Primärschlüssel für die Tabelle Benutzung fest, aber die PDF sagt nicht, wie die SessionID generiert wird oder ob sie eindeutig ist oder nicht. Es wäre besser, zu erklären, wie die SessionID vergeben wird oder eine andere Möglichkeit zu finden, die Benutzungen zu identifizieren.
- Der Code verwendet Fremdschlüsselbeziehungen zwischen den Tabellen, aber die PDF sagt nicht, welche Art von Beziehungen zwischen den Entitäten bestehen. Es wäre besser, ein Entity-Relationship-Diagramm (ERD) zu erstellen oder zu beschreiben, wie die Entitäten miteinander verbunden sind.
- Der Code verwendet das Datum als Datentyp für LoggIn und LoggOut in der Tabelle Benutzung, aber das Datum enthält keine Zeitinformationen. Es wäre besser, einen Datentyp zu verwenden, der sowohl das Datum als auch die Zeit speichern kann, z.B. DATETIME oder TIMESTAMP.
- Der Code erfüllt die erste Normalform (1NF), da alle Attribute atomar sind und jede Tabelle einen Primärschlüssel hat. Der Code erfüllt auch die zweite Normalform (2NF), da alle Attribute von den gesamten Primärschlüsseln abhängig sind und es keine partiellen Abhängigkeiten gibt. Der Code erfüllt jedoch nicht die dritte Normalform (3NF), da es transitive Abhängigkeiten gibt. Zum Beispiel hängt das Attribut Klasse in der Tabelle Benutzer transitiv vom Attribut Name ab. Es wäre besser, eine separate Tabelle für Klassen zu erstellen und eine Fremdschlüsselbeziehung zwischen Benutzer und Klasse herzustellen.

## 2.

-----------------------

[Ich habe einige Web-Suchergebnisse gefunden, die Ihnen nützlich sein könnten](https://nanonets.com/blog/pdf-to-database/)[1](https://nanonets.com/blog/pdf-to-database/)[2](https://docparser.com/blog/pdf-to-database/)[3](https://www.techwalla.com/articles/how-to-set-up-a-searchable-pdf-database)[4](https://itstillworks.com/store-pdf-files-database-7447329.html). [Außerdem habe ich eine Frage-Antwort gefunden, die erklärt, wie man eine durchsuchbare PDF-Datenbank einrichtet](https://www.techwalla.com/articles/how-to-set-up-a-searchable-pdf-database)[3](https://www.techwalla.com/articles/how-to-set-up-a-searchable-pdf-database).

Ich habe den Code analysiert und festgestellt, dass er einige der Aufgaben aus der PDF-Datei erfüllt, aber auch einige Probleme hat, die behoben werden müssen. Hier sind einige Punkte, die Sie beachten sollten:

- Der Code erstellt eine Datenbank namens DBS und vier Tabellen: Rechner, Raum, Benutzer und Benutzung. Das entspricht den Aufgaben 1 und 2 aus der PDF-Datei.
- Der Code verwendet den Datentyp TEXT für einige Spalten, die besser als VARCHAR oder CHAR definiert werden sollten. Zum Beispiel könnte die Spalte Hersteller in der Tabelle Rechner als VARCHAR(20) definiert werden, um Speicherplatz zu sparen und die Leistung zu verbessern. Das entspricht der Aufgabe 3 aus der PDF-Datei.
- Der Code verwendet den Datentyp DATE für die Spalten Baujahr, LoggIn und LoggOut. Das ist nicht korrekt, da diese Spalten auch die Uhrzeit enthalten sollten. Der richtige Datentyp wäre DATETIME oder TIMESTAMP. Das entspricht der Aufgabe 4 aus der PDF-Datei.
- Der Code verwendet IDENTITY für alle Primärschlüssel-Spalten. Das ist nicht nötig, da einige dieser Spalten bereits eindeutige Werte haben, wie zum Beispiel Raumnummer oder Seriennummer. Das entspricht der Aufgabe 5 aus der PDF-Datei.
- Der Code verwendet FOREIGN KEY REFERENCES ohne ON DELETE oder ON UPDATE Optionen. Das bedeutet, dass die referenziellen Aktionen standardmäßig auf RESTRICT gesetzt sind, was zu Problemen führen kann, wenn man Daten löschen oder aktualisieren will. Es wäre besser, explizit anzugeben, welche Aktionen bei Löschung oder Aktualisierung erfolgen sollen, wie zum Beispiel CASCADE oder SET NULL. Das entspricht der Aufgabe 6 aus der PDF-Datei.



## 3.

--------------------------

Um die Anmeldungen von Benutzern an Rechnern im letzten Monat abzufragen, können Sie eine SQL-Abfrage verwenden, die die Tabellen Benutzer, Rechner und Benutzung verbindet und die LoggIn-Daten mit dem letzten Monat vergleicht. Zum Beispiel, um die Anmeldungen von Hans Müller am Rechner 3 zu finden, können Sie diese Abfrage verwenden:

```sql
SELECT Benutzung.LoggIn, Benutzung.LoggOut
FROM Benutzer
JOIN Benutzung ON Benutzer.ID = Benutzung.FK_BenutzerID
JOIN Rechner ON Rechner.Seriennummer = Benutzung.FK_Seriennummer
WHERE Benutzer.VorName = 'Hans' AND Benutzer.NachName = 'Müller'
AND Rechner.Seriennummer = 3
AND Benutzung.LoggIn BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 1, 0) -- erster Tag des letzten Monats
AND DATEADD(MILLISECOND, -3, DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) -- letzter Tag des letzten Monats
```

Diese Abfrage gibt die LoggIn- und LoggOut-Zeiten von Hans Müller am Rechner 3 im letzten Monat zurück. Wenn Sie die Anmeldungen von allen Benutzern an allen Rechnern im letzten Monat sehen wollen, können Sie die WHERE-Bedingungen entsprechend anpassen oder weglassen.

Ich habe die Funktionen DATEADD und DATEDIFF verwendet, um den Datumsbereich des letzten Monats zu berechnen. Sie können mehr über diese Funktionen in den folgenden Links erfahren: [DATEADD](https://www.timeanddate.com/date/duration.html), [DATEDIFF](https://stackoverflow.com/questions/1114592/getdate-last-month).



## 4.

-----------------------------

Um Testdatensätze für diese Datenbank zu generieren, können Sie verschiedene Methoden verwenden, je nachdem, welche Art von Daten Sie benötigen. Eine Möglichkeit ist, ein Werkzeug wie [Mockaroo](https://mockaroo.com/) zu verwenden, das Ihnen erlaubt, große Mengen von realistischen Testdaten basierend auf Ihren eigenen Spezifikationen zu erstellen und diese im SQL- oder CSV-Format herunterzuladen. Sie können die Feldnamen, Datentypen und Optionen für jede Tabelle anpassen und auch Beziehungen zwischen den Tabellen definieren. Mockaroo ist einfach zu bedienen und erfordert keine Programmierung.

Eine andere Möglichkeit ist, T-SQL-Skripte zu schreiben, die zufällige Daten mit Hilfe von Funktionen wie [dbms_random](https://stackoverflow.com/questions/19398787/generate-test-data-using-oracle-pl-sql-developer) oder [DATEADD](https://mockaroo.com/) und [DATEDIFF](https://www.red-gate.com/hub/product-learning/sql-data-generator/generate-fake-test-data-sql-server) erzeugen. Diese Funktionen können Ihnen helfen, zufällige Werte für verschiedene Datentypen wie Text, Zahlen oder Datum zu generieren. Sie können auch Schleifen oder rekursive Abfragen verwenden, um mehrere Zeilen in einer Tabelle einzufügen. Ein Beispiel für ein solches Skript finden Sie in diesem [Artikel](https://www.sqlshack.com/how-to-generate-random-sql-server-test-data-using-t-sql/).